Name:     douda_config
Version:  0.8
Release:  1
Summary:  Some personal configuration files
License:  GPLv3+
URL:      http://nope.nope
Source0:  dotfiles.tar.bz2
Requires: accountsservice, xorg-x11-server-Xorg, sudo, sddm
Requires(pre): shadow-utils

%global debug_package %{nil}
%global _python_bytecompile_errors_terminate_build 0

%prep
%setup -q -n dotfiles

%pre
getent group  chedi >/dev/null || groupadd chedi
getent passwd chedi >/dev/null || useradd -g chedi -p yofL/LtYrY7CI -M chedi

%install
rm -rf     $RPM_BUILD_ROOT/*
cp -r home $RPM_BUILD_ROOT
%{__mkdir} -p %{buildroot}%{_datadir}/%{name}
cp -rT system %{buildroot}%{_datadir}/%{name}

%posttrans
cat >> /var/lib/AccountsService/users/chedi << EOF
[User]
Language=en_US.utf8
XSession=xmonad
Icon=/var/lib/AccountsService/icons/chedi
SystemAccount=false
EOF

cat >> /etc/X11/xorg.conf.d/00-keyboard.conf << EOF
Section "InputClass"
        Identifier "system-keyboard"
        MatchIsKeyboard "on"
        Option "XkbLayout" "fr"
EndSection
EOF

# Remove the password requirement for the sudoers
sed -i -e "s/^\(%wheel.*\)ALL$/\1 NOPASSWD: ALL/" /etc/sudoers

cp /home/chedi/Pictures/Avatar/chedi /var/lib/AccountsService/icons/
tar xjf %{_datadir}/%{name}/.emacs.d.tar.bz2   -C /home/chedi
tar xjf %{_datadir}/%{name}/virtualenv.tar.bz2 -C /home/chedi/.cache
tar xjf %{_datadir}/%{name}/Org.tar.bz2        -C /home/chedi/Documents
chown -R chedi.chedi /home/chedi
usermod -aG wheel chedi

%clean
PYTHON_DISALLOW_AMBIGUOUS_VERSION=0
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)
%{_datadir}/%{name}
/home/chedi

%triggerin -- sddm
install -m 0644 %{_datadir}/%{name}/sddm.conf %{_sysconfdir}/sddm.conf
cp -ar %{_datadir}/%{name}/chili %{_datadir}/sddm/themes/

%description
Various configuration files

%changelog
* Thu Jul 12 2018 Douda <chedi.toueiti@gmail.com> - 0.8
- using ssdm as default login greater
* Tue Jul 10 2018 Douda <chedi.toueiti@gmail.com> - 0.7
- updating to fedora 28
* Sat Dec 03 2016 Douda <chedi.toueiti@gmail.com> - 0.6
- updating to fedora 25
* Tue Nov 10 2015 Douda <chedi.toueiti@gmail.com> - 0.5
- updating to fedora 23
* Wed Sep 23 2015 Douda <chedi.toueiti@gmail.com> - 0.4
- updating to fedora 23 beta
* Mon May 25 2015 Douda <chedi.toueiti@gmail.com> - 0.3
- updating the package for fedora 22
* Thu Feb 12 2015 Douda <chedi.toueiti@gmail.com> - 0.1
- Initial version of the package
